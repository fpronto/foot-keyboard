#![no_std]
#![no_main]

fn main() {
    // It is necessary to call this function once. Otherwise some patches to the runtime
    // implemented by esp-idf-sys might not link properly. See https://github.com/esp-rs/esp-idf-template/issues/71
    esp_idf_svc::sys::link_patches();

    log::info!("Hello, world!");
    log::info!("Hello, from foot-keyboard!");
}
